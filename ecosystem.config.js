/* eslint no-multi-str: [0] */
const APP_NAME = 'MT6_Server'
const PATH = '/home/dabank/test/server'
const USER = 'dabank'
const REPO = 'git@gitlab.com:binh.cq/test-socket.git'

const DEV_IP = '128.199.176.204'
const DEV_PORT = '2323'
const DEV_BRANCH = 'origin/dev'

const POST_DEPLOY = {
  DEV: 'ln -nfs ../shared/.env .env && \
          npm install && \
          pm2 reload ecosystem.config.js',
}

const PRE_DEPLOY = 'git checkout package-lock.json'

module.exports = {
  apps: [{
    name: APP_NAME,
    script: './app.js',
    instances: 'max',
    exec_mode: 'cluster'
  }],

  deploy: {
    dev: {
      user: USER,
      host: [{
        host: DEV_IP,
        port: DEV_PORT
      }],
      ref: DEV_BRANCH,
      repo: REPO,
      path: PATH,
      'pre-deploy': PRE_DEPLOY,
      'post-deploy': POST_DEPLOY.DEV
    }
  }
}
