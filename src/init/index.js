import moment from 'moment'
import config from '../configs'

const socketIO = require('../modules/socketIO/emitter')

export default async () => {
  require('./misc')
  if (global.isCronJobServer) {
    emitCountdown()
  }
}

function emitCountdown() {
  let status = moment().get('s') < 30 ? 'ping' : 'pong'
  setInterval(() => {
    let time = moment().get('s')
    if (time < 30) {
      time = 30 - time
      socketIO.of(config.socket.namespace.user).emit(config.socket.events.countDown, {
        time, status
      })
      if (time === 1) {
        status = 'pong'
      }
    } else {
      time = 60 - time
      socketIO.of(config.socket.namespace.user).emit(config.socket.events.countDown, {
        time, status
      })
      if (time === 1) {
        status = 'ping'
      }
    }
  }, 1000)
}
