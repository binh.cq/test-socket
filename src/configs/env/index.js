const app = {
  env: {
    production: 'production',
    development: 'development',
    test: 'test'
  },

  conventions: {
    number: 0,
    array: [],
    string: '',
    object: null,
    allValue: 'all'
  },

  regex: {
    objectId: /^[0-9a-fA-F]{24}$/,
    phone: /^\+?1?(\d{10,12}$)/,
    email: /\S+@\S+\.\S+/,
    password: /^[a-f0-9]{32}$/,
    address: /^0x[a-fA-F0-9]{40}$/,
    signature: /^0x[a-fA-F0-9]{130}$/,
    authenticate: /^\d{6}$/
  },

  format: {
    date: 'DD/MM/YYYY, HH:mm'
  },

  socket: {
    events: {
      countDown: 'countDown',
    },
    namespace: {
      admin: '/admin',
      member: '/members',
      user: '/users'
    }
  },
}

export default app
