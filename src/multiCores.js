import http from 'http'
import socketIO from './modules/socketIO'

export default (app, statusMiddleware, mediator) => {
  const server = http.createServer(app)

  socketIO.connect(server)
  server.listen(3009)
  if (process.env.NODE_APP_INSTANCE === '0') {
    console.log('*************************************************')
    console.log(`*** THIS IS A CRONJOB SERVER WITH WORKER ID ${process.env.NODE_APP_INSTANCE} ***`)
    console.log('*************************************************')
    global.isCronJobServer = true
    global.isIndexesServer = true
  }
  setImmediate(() => {
    mediator.emit('boot.ready')
  })

  server.on('error', onError)
  handleSigInt(server)
  handleSigTerm(server, statusMiddleware)
  handleMessages()
}

function handleSigInt(server) {
  process.on('SIGINT', () => {
    console.info('SIGINT signal received.')

    server.close((err) => {
      if (err) {
        console.error(err)
        process.exit(1)
      }
    })
  })
}

function handleSigTerm(server, statusMiddleware) {
  process.on('SIGTERM', () => {
    statusMiddleware.shutdown()
      .then(() => {
        server.close()
      })
  })
}

function handleMessages() {
  process.on('message', (msg) => {
    if (msg === 'shutdown') {
      console.log('Closing all connections...')
      setTimeout(() => {
        console.log('Finished closing connections')
        process.exit(0)
      }, 1500)
    }
  })
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  switch (error.code) {
    case 'EACCES':
      console.error(`Pipe ${process.env.PORT} requires elevated privileges`)
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(`Port ${process.env.PORT} is already in use`)
      process.exit(1)
      break
    default:
      throw error
  }
}
