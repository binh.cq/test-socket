import express from 'express'
import ev from 'express-validation'
import morgan from 'morgan'
import cors from 'cors'
import methodOverride from 'method-override'
import compress from 'compression'
import helmet from 'helmet'
import { EventEmitter } from 'events'
import bodyParser from 'body-parser'
import httpchk from 'express-httpchk'
import multiCores from './multiCores'
import init from './init'
import route from './routes'
import logger from './logger'

const mediator = new EventEmitter()
const app = express()

app.use(morgan('dev'))
app.use(cors())
app.use(compress())
app.use(methodOverride())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(helmet())

function getStatus() {
  return { status: 'OK' }
}

const statusMiddleware = httpchk(getStatus)

app.get('/status', statusMiddleware)

multiCores(app, statusMiddleware, mediator)

mediator.once('boot.ready', async () => {
  await init(app)
  app.use(route())
  app.use(handleNotFoundError)
  app.use(handleValidationError)
  console.log('SERVER BOOT READY')
})

function handleNotFoundError(req, res) {
  console.log('404', req.url)
  logger.info({
    data: {
      url: `404 - ${req.method.toUpperCase()} ${req.url}`,
      clientData: ['get', 'delete'].includes(req.method.toLowerCase()) ? req.query : req.body
    }
  })
  return res.status(404)
}

// eslint-disable-next-line no-unused-vars
function handleValidationError(error, req, res, _) {
  if (error instanceof ev.ValidationError) {
    return res.status(error.status)
  } else {
    console.log('500', error)
    logger.info({
      data: {
        url: `500 - ${req.method.toUpperCase()} ${req.url}`,
        clientData: ['get', 'delete'].includes(req.method.toLowerCase()) ? req.query : req.body
      }
    })
    return res.status(500)
  }
}

export default app
