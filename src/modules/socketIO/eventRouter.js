import config from '../../configs'

require('colors')

async function init(io) {
  io.of(config.socket.namespace.user).on(config.socket.events.connection, (socket) => {
    handleMemberUserConnection(io, socket)
  })
}

async function handleMemberUserConnection(io, socket) {
  console.log(`New socket connected ${socket.id}`.green)
  socket.join(config.socket.events.countDown)
  socket.on('disconnect', () => {
    console.log(`Client disconnected: ${socket.id}`.green)
  })
}

export default {
  init
}
