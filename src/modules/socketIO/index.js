import eventRouter from './eventRouter'

const redisAdapter = require('socket.io-redis')
const Redis = require('ioredis')

function connect(server) {
  const io = require('socket.io')(server)
  io.adapter(redisAdapter(new Redis({ host: process.env.REDIS_IP || 'localhost', port: 6379 })))
  eventRouter.init(io)
}

export default {
  connect
}
