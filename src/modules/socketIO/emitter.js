module.exports = require('socket.io-emitter')({ host: process.env.REDIS_IP || 'localhost', port: 6379 })
